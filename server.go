package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/julienschmidt/httprouter"
)

var bucket, siteName string
var debug bool
var sess *session.Session
var client *s3.S3

func main() {

	var serverPort, prettyPort, prettyPath string
	var router *httprouter.Router

	// Get config values from command line flags.
	flag.StringVar(&bucket, "bucket", "", "S3 bucket name")
	flag.StringVar(&serverPort, "port", "8090", "Port for s3miniproxy to run on")
	flag.StringVar(&siteName, "name", "", "Name of the site")
	flag.BoolVar(&debug, "debug", false, "Enable debugging")
	flag.Parse()

	if bucket == "" {
		log.Fatal("No bucket specified!")
	}

	prettyPort = fmt.Sprintf("127.0.0.1:%s", serverPort)
	prettyPath = fmt.Sprintf("/%s/*filepath", siteName)

	log.Printf("Starting server on port %s...", serverPort)
	// Get the global session and client.
	sess = session.Must(session.NewSession())
	client = s3.New(sess)

	// Setup the webserver.
	router = httprouter.New()
	router.GET(prettyPath, ProxyHandler)
	log.Fatal(http.ListenAndServe(prettyPort, router))
}

// ProxyHandler - Handle the web proxying to s3 bucket.
func ProxyHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var ifNoneMatch, ifModifiedSince, key, etag string
	var lm *time.Time
	var ifModDate time.Time
	var getObjCfg s3.GetObjectInput
	var result *s3.GetObjectOutput
	var body []byte
	var err error

	// Grab cachingheaders from request, if any.
	ifNoneMatch = r.Header.Get("If-None-Match")
	ifModifiedSince = r.Header.Get("If-Modified-Since")

	// Convert the path of the request to the key.
	key = r.URL.Path
	DebugLog("DEBUG: key received is %s", key)
	// Assume that incoming requests will be /siteName/key or /key
	// So remove leading /siteName, then remove leading / so we are
	// Left with just `key`.
	key = strings.TrimPrefix(key, fmt.Sprintf("/%s", siteName))
	DebugLog("DEBUG: trimmed key received is %s", key)
	if len(key) > 0 {
		key = key[1:]
	}
	// Assume index if nothing given.
	if key == "" {
		key = "index.html"
	}
	DebugLog("DEBUG: modified key received is %s", key)

	// Compile the request.
	getObjCfg = s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	}
	switch {
	case ifNoneMatch != "":
		getObjCfg.SetIfNoneMatch(ifNoneMatch)
	case ifModifiedSince != "":
		ifModDate, err = http.ParseTime(ifModifiedSince)
		if err == nil {
			getObjCfg.SetIfModifiedSince(ifModDate)
		}
	}

	// Perform request.
	result, err = client.GetObject(&getObjCfg)

	// Check error. This is a little bizarre, but
	// it checks the error type to see if its an aws sdk error or not.
	// Then reads for specific error.
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case "NotModified":
				etag = *result.ETag
				if etag != "" {
					w.Header().Set("ETag", etag)
				}
				lm = result.LastModified
				w.Header().Set("Last-Modified", lm.Format(http.TimeFormat))
				if result.ContentType != nil {
					w.Header().Set("Content-Type", *result.ContentType)
				}
				w.WriteHeader(304)
				return
			case s3.ErrCodeNoSuchKey:
				w.WriteHeader(404)
				return
			default:
				log.Printf("Error occurred with key %s: %s", key, err)
				w.WriteHeader(500)
				return
			}
		}
	}

	// It worked if we got this far. Now read the object and
	// set headers for caching.
	DebugLog("DEBUG: Etag is %s", result.ETag)

	if result.ETag != nil {
		etag = *result.ETag
		w.Header().Set("ETag", etag)
	}
	lm = result.LastModified
	if lm != nil {
		w.Header().Set("Last-Modified", lm.Format(http.TimeFormat))
	}
	if result.ContentType != nil {
		w.Header().Set("Content-Type", *result.ContentType)
	}
	// Read and write the object.
	defer result.Body.Close()
	body, err = ioutil.ReadAll(result.Body)
	if err != nil {
		w.WriteHeader(500)
	}
	_, _ = w.Write(body)
}

// DebugLog - log if debug is set.
func DebugLog(format string, a ...interface{}) {
	if debug {
		log.Printf(format, a)
	}
}
